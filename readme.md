# Thème du site git.spip.net

## Introduction

> Le dépot permet de personnaliser la mise en forme du site git.spip.net

## Documentation

La structure des squelettes et autres éléments sont détaillées sur la documentation officielle de gitea :
* https://docs.gitea.io/en-us/customizing-gitea/

Les gabarits surchargeables sont définis dans le dépot source :
* https://github.com/go-gitea/gitea/tree/master/templates


## Contribuer

Les correctifs sont avec plaisir appréciés et bienvenus.
Pour contribuer il est conseillé d'utiliser la version [git](https://git.spip.net/galaxie/git.spip.net).

### Git ou SVN ?

Le code est disponible sur :
* [git](https://git.spip.net/galaxie/git.spip.net)
* [svn](https://zone.spip.net/trac/spip-zone/browser/spip-zone/_galaxie_/git.spip.net).

Ces 2 versions sont synchronisées et par conséquent il est normalement possible de lire et écrire indifféremment sur les 2 dépots.
**Toutefois** c'est la version git qui sert de référence. 
Si pour une raison quelleconque la synchronisation est cassée, ce sera git qui aura le dernier mot.
